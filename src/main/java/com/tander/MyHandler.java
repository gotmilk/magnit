package com.tander;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class MyHandler extends DefaultHandler {

	Long sumValue = 0l;
	boolean bEntries = false;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("entry")) {
			String fieldNo = attributes.getValue("field");
			sumValue += Long.valueOf(fieldNo);
		} else if (qName.equalsIgnoreCase("entries")) {
			bEntries = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase("entries")) {
			System.out.println("XML sum: " + sumValue);
		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {
		// TODO:..
	}
}