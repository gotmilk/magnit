package com.tander;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public abstract class Utils {
	public static void generateXML(List<Integer> list, String fileName) {

		try {
			OutputStream outputStream = new FileOutputStream(new File(fileName));

			XMLStreamWriter out = XMLOutputFactory.newInstance()
					.createXMLStreamWriter(new OutputStreamWriter(outputStream, "utf-8"));

			out.writeStartDocument();
			out.writeStartElement("entries");

			for (Integer i : list) {
				out.writeStartElement("entry");
				out.writeStartElement("field");
				out.writeCharacters(i.toString());
				out.writeEndElement();
				out.writeEndElement();
			}

			out.writeEndElement();
			out.writeEndDocument();

			out.close();

			System.out.println("File " + fileName + " saved!");

		} catch (UnsupportedEncodingException e) {

		} catch (XMLStreamException e) {

		} catch (FileNotFoundException e) {

		}
	}

	public static void generateXMLUsingXSLT(InputStream XSLTStream, String fileInputName, String fileOutputName) {
		try {
			TransformerFactory factory = TransformerFactory.newInstance();
			Source xslt = new StreamSource(XSLTStream);
			Transformer transformer;
			transformer = factory.newTransformer(xslt);

			Source text = new StreamSource(new File(fileInputName));
			transformer.transform(text, new StreamResult(new File(fileOutputName)));

			System.out.println("File " + fileOutputName + " saved!");

		} catch (TransformerConfigurationException e) {
			//
		} catch (TransformerException e) {
			//
		}
	}

	public static void calculateSum(String fileName) {
		try {
			File inputFile = new File(fileName);
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			MyHandler userhandler = new MyHandler();
			saxParser.parse(inputFile, userhandler);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
