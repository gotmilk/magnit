package com.tander;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainBean {

	private String url;
	private String username;
	private String password;
	private Integer n;

	private static final String SQL_DELETE = "DELETE FROM `table`";
	private static final String SQL_INSERT = "INSERT INTO `table` (FIELD) VALUES (?)";
	private static final String SQL_RETRIEVE = "SELECT FIELD FROM `table`";

	public void setUrl(String url) {
		this.url = url;
	}

	public void setN(Integer n) {
		this.n = n;
	}

	public String getUrl() {
		return this.url;
	}

	public Integer getN() {
		return this.n;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private PreparedStatement insertBatch(PreparedStatement preparedStatement) throws SQLException {
		
		// long executeCounter = 0;
		
		for (int i = 1; i < this.getN() + 1; i++) {
			preparedStatement.setInt(1, i);
			preparedStatement.addBatch();
			
			// executeCounter++;
			// if (executeCounter >= 10000) {
			// preparedStatement.executeBatch(); statement.clearBatch();
			// }
			
		}
		
		// if (executeCounter > 0) {
		// preparedStatement.executeBatch();
		// }
		
		return preparedStatement;
	}

	private List<Integer> retrieveFields(PreparedStatement preparedStatement) throws SQLException {
		List<Integer> fieldsList = new ArrayList<>();
		ResultSet rs = preparedStatement.executeQuery();
		while (rs.next()) {
			Integer field = rs.getInt("FIELD");
			fieldsList.add(field);
		}

		return fieldsList;
	}

	public void generateXMLFiles() {

		try (Connection connection = DriverManager.getConnection(this.getUrl(), this.getUsername(), this.getPassword());
				PreparedStatement stmtDelete = connection.prepareStatement(SQL_DELETE);
				PreparedStatement stmtInsert = connection.prepareStatement(SQL_INSERT);
				PreparedStatement stmtRetrieve = connection.prepareStatement(SQL_RETRIEVE)) {

			connection.setAutoCommit(false);

			// Execute DELETE
			stmtDelete.executeUpdate();

			// Execute INSERT
			insertBatch(stmtInsert).executeBatch();

			connection.commit();

			// Execute SELECT
			List<Integer> fieldsList = retrieveFields(stmtRetrieve);

			// Generate 1.xml
			Utils.generateXML(fieldsList, "1.xml");

			// Generate 2.xml
			InputStream is = this.getClass().getClassLoader().getResourceAsStream("transform.xsl");
			Utils.generateXMLUsingXSLT(is, "1.xml", "2.xml");

			// Calculate 2.xml
			Utils.calculateSum("2.xml");

		} catch (SQLException e) {
			
			// TODO: rollback
			System.out.println("Error while proceeding");
		}
	}
}
