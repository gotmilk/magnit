package com.tander;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

	public static void main(String[] args) throws IOException {

		MainBean bean = new MainBean();
		bean.setUrl(
				"jdbc:mysql://192.168.1.191:3306/tander?useLegacyDatetimeCode=false&serverTimezone=America/New_York");

		bean.setUsername("konstantin"); 
		bean.setPassword("test");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter N:");
		int n = 0;
		try {
			n = Integer.parseInt(br.readLine());
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format of N!");
		}
		bean.setN(n);

		long start = System.currentTimeMillis();
		bean.generateXMLFiles();
		long finish = System.currentTimeMillis();

		long timeConsumedMs = finish - start;
		System.out.println("Run time: " + timeConsumedMs + " ms = " + (double) timeConsumedMs / 60000 + " min");

	}
}
